from mathutils.geometry import intersect_point_line
from src.line import Line
from src.coordinate import Coordinate

def get_nearest_point_on_line(point, line):
    intersect = intersect_point_line(point, line[0], line[1])


class PolyLine:

    def __init__(self, line):
        self.maxX = line.start.x if line.start.x > line.end.x else line.end.x
        self.maxY = line.start.y if line.start.y > line.end.y else line.end.y
        self.minX = line.end.x if line.start.x > line.end.x else line.start.x
        self.minY = line.end.y if line.start.y > line.end.y else line.start.y
        self.lines = [line]

    def append(self, coord):
        self.maxX = coord.x if coord.x > self.maxX else self.maxX
        self.maxY = coord.y if coord.y > self.maxY else self.maxY
        self.minX = coord.x if coord.x < self.minX else self.minX
        self.minY = coord.y if coord.y < self.minY else self.minY
        self.lines.append(Line(self.lines[-1].end, coord))

    def get_bound_centre(self):
        coordinate = Coordinate((self.maxX - self.minX) / 2 + self.minX, (self.maxY - self.minY) / 2 + self.minY)
        return coordinate

    def get_nearest_point_on_poly_line(self):
        centre = self.get_bound_centre()
        for line in self.lines:
            get_nearest_point_on_line(centre, line)


    def __str__(self):
        return str(self.lines)

    def __repr__(self):
        return str(self)
