import fileinput
from src.coordinate import Coordinate
from src.line import Line
from src.poly_line import PolyLine

lines = []
for line in fileinput.input():
    lines.append(line.rstrip())


newLines = []
for line in map(lambda x: x.split(" "), lines):
    coordinates = []
    for i in range(int(len(line)/2)):
        coordinates.append(Coordinate(int(line[i*2]), int(line[i*2 + 1])))
    newLines.append(coordinates)

polyLines = []
for line in newLines:
    start = Line(line[0], line[1])
    polyLine = PolyLine(start)
    for coord in line[2:]:
        polyLine.append(coord)
    polyLines.append(polyLine)

for polyLine in polyLines:
    print(polyLine.get_bound_centre())
    print(polyLine)
